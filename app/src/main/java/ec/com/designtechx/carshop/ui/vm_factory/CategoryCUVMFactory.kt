package ec.com.designtechx.carshop.ui.vm_factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ec.com.designtechx.carshop.data.domain.repo.RepoCategory
import ec.com.designtechx.carshop.provider.CategoryProvider
import ec.com.designtechx.carshop.ui.view_model.CategoryCUVM

class CategoryCUVMFactory :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CategoryCUVM(RepoCategory(CategoryProvider.instance!!)) as T
    }
}