package ec.com.designtechx.carshop.utils

class Constants {
    companion object{
        const val KEY_CATEGORY = "KEY_CATEGORY"
        const val KEY_CAR = "KEY_CAR"
    }
}