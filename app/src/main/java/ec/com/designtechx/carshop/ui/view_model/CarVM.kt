package ec.com.designtechx.carshop.ui.view_model

import androidx.lifecycle.ViewModel
import com.firebase.ui.database.FirebaseRecyclerOptions
import ec.com.designtechx.carshop.data.domain.irepo.IRepoCar
import ec.com.designtechx.carshop.data.domain.irepo.IRepoCategory
import ec.com.designtechx.carshop.data.model.Car
import ec.com.designtechx.carshop.data.model.Category
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect

class CarVM(private val iRepoCar: IRepoCar) :
    ViewModel() {

    private val _category = MutableStateFlow(Category())
    val category: StateFlow<Category> get() = _category

    fun delete(key: String) {
        iRepoCar.delete(key)
    }

    fun getOptionsCar(): FirebaseRecyclerOptions<Car> {
        return iRepoCar.getOptions()
    }
}