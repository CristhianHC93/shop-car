package ec.com.designtechx.carshop.ui.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import ec.com.designtechx.carshop.databinding.ActivitySplashScreenBinding
import ec.com.designtechx.carshop.ui.view_model.SplashScreenVM
import ec.com.designtechx.carshop.ui.vm_factory.SplashScreenVMFactory
import ec.com.designtechx.carshop.utils.Results
import kotlinx.coroutines.flow.collect

class SplashScreenActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySplashScreenBinding

    private val viewModel by lazy {
        ViewModelProvider(this, SplashScreenVMFactory()).get(SplashScreenVM::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        configBiding()
        listenVM()
    }

    private fun listenVM() {
        lifecycleScope.launchWhenStarted {
            viewModel.existSession.collect {
                when (it) {
                    Results.OK -> openMainActivity()
                    Results.NOT_EXISTENT -> openLoginActivity()
                    Results.NOT_CONFIRM -> openLoginActivity()
                    else -> {
                    }
                }
            }
        }
    }

    private fun openLoginActivity() {
        startActivity(Intent(applicationContext, LoginActivity::class.java))
        finish()
    }

    private fun openMainActivity() {
        startActivity(Intent(applicationContext, MainActivity::class.java))
        finish()
    }


    private fun configBiding() {
        binding = ActivitySplashScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}