package ec.com.designtechx.carshop.ui.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import ec.com.designtechx.carshop.databinding.FragmentCategoryBinding
import ec.com.designtechx.carshop.ui.activity.CategoryCUActivity
import ec.com.designtechx.carshop.ui.activity.MainActivity
import ec.com.designtechx.carshop.ui.adapter.CategoryAdapter
import ec.com.designtechx.carshop.ui.view_model.CategoryVM
import ec.com.designtechx.carshop.ui.vm_factory.CategoryVMFactory

class CategoryFragment : Fragment() {
    private var _binding: FragmentCategoryBinding? = null
    private val binding get() = _binding!!

    private val viewModel by lazy {
        ViewModelProvider(this, CategoryVMFactory()).get(CategoryVM::class.java)
    }

    private lateinit var categoryAdapter:CategoryAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCategoryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        eventClick()
    }

    private fun eventClick() {
        binding.btAdd.setOnClickListener { openActivityCategory() }
    }

    private fun openActivityCategory() {
        val intent = Intent(context, CategoryCUActivity::class.java)
        requireContext().startActivity(intent)
    }

    override fun onStart() {
        super.onStart()
        (requireActivity() as MainActivity).binding.progressBar.visibility = View.VISIBLE
        configRecyclerAdapter()
    }

    private fun configRecyclerAdapter() {
        val linearLayoutManager = LinearLayoutManager(requireContext())
        binding.rvCategory.layoutManager = linearLayoutManager
        categoryAdapter = CategoryAdapter(viewModel.getOptionsCategory(), requireContext(),viewModel)
        binding.rvCategory.adapter = categoryAdapter
        categoryAdapter.startListening()
        (requireActivity() as MainActivity).binding.progressBar.visibility = View.INVISIBLE
    }

    override fun onStop() {
        super.onStop()
        categoryAdapter.stopListening()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}