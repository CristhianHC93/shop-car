package ec.com.designtechx.carshop.ui.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import ec.com.designtechx.carshop.R
import ec.com.designtechx.carshop.databinding.ActivityMainBinding
import ec.com.designtechx.carshop.ui.view_model.MainVM
import ec.com.designtechx.carshop.ui.vm_factory.MainVMFactory
import ec.com.designtechx.carshop.utils.GeneralHelper

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding

    private val viewModel by lazy {
        ViewModelProvider(this, MainVMFactory()).get(MainVM::class.java)
    }

    private lateinit var navController: NavController
    private val generalHelper by lazy { GeneralHelper.getInstance(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        configBinding()
        setupToolBarAndNavigation()
    }

    private fun configBinding() {
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    private fun setupToolBarAndNavigation() {
        setSupportActionBar(binding.toolBar)
        val navHostFragment =
            supportFragmentManager.findFragmentById(binding.navMainFragment.id) as NavHostFragment
        navController = navHostFragment.navController
        NavigationUI.setupActionBarWithNavController(this, navController)
        NavigationUI.setupWithNavController(
            binding.bttmNav,
            navHostFragment.navController
        )
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        val listDrawable = listOf(
            menu.findItem(R.id.exit).icon,
            menu.findItem(R.id.profile).icon,
        )
        generalHelper.setColorItemMenu(
            listDrawable,
            ContextCompat.getColor(applicationContext, R.color.purple_500)
        )
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.exit -> exitToLogin()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun exitToLogin() {
        viewModel.logout()
        startActivity(Intent(this, LoginActivity::class.java))
    }
}