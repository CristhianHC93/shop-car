package ec.com.designtechx.carshop.ui.view_model

import androidx.lifecycle.ViewModel
import ec.com.designtechx.carshop.data.domain.irepo.IRepoUser
import ec.com.designtechx.carshop.data.model.User
import ec.com.designtechx.carshop.utils.Results
import ec.com.designtechx.taxidriver.data.irepo.IRepoAuth
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect

class RegisterVM(private val iRepoAuth: IRepoAuth, private val iRepoUser: IRepoUser) : ViewModel() {
    private val _createResult = MutableStateFlow(Results.UNANSWERED)
    val createResult: StateFlow<Results> get() = _createResult

    suspend fun register(user: User) {
        iRepoAuth.register(user.email, user.password)
            .collect {
                if (it.isNotEmpty()) createUser(user.apply { id = it })
                else _createResult.value = Results.FAIL_REGISTER
            }
    }

    private suspend fun createUser(user: User) {
        iRepoUser.create(user)
            .collect {
                if (it) sendEmailConfirmation() else _createResult.value = Results.FAIL_CREATE
            }
    }

    private suspend fun sendEmailConfirmation() {
        iRepoAuth.sendEmailConfirmation().collect { _createResult.value = if (it) Results.OK else Results.FAIL_SEND_CONFIRMATION }
    }

    fun resetResult() {
        _createResult.value = Results.UNANSWERED
    }
}