package ec.com.designtechx.carshop.data.domain.irepo

import com.firebase.ui.database.FirebaseRecyclerOptions
import ec.com.designtechx.carshop.data.model.Category
import kotlinx.coroutines.flow.Flow

interface IRepoCategory {
    suspend fun create(category: Category): Flow<Boolean>
    suspend fun update(category: Category, key: String): Flow<Boolean>
    fun getOptions(): FirebaseRecyclerOptions<Category>
    suspend fun getCategory(key:String):Flow<Category>
    fun getCategorySingle(key: String):Category
    fun delete(key:String)
}