package ec.com.designtechx.carshop.data.domain.irepo

import com.firebase.ui.database.FirebaseRecyclerOptions
import ec.com.designtechx.carshop.data.model.Car
import kotlinx.coroutines.flow.Flow

interface IRepoCar {
    suspend fun create(car: Car): Flow<Boolean>
    suspend fun update(car: Car, key: String): Flow<Boolean>
    fun getOptions(): FirebaseRecyclerOptions<Car>
    suspend fun getCar(key: String): Flow<Car>
    fun delete(key: String)
}