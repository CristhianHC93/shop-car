package ec.com.designtechx.carshop.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import ec.com.designtechx.carshop.R
import ec.com.designtechx.carshop.data.model.User
import ec.com.designtechx.carshop.databinding.FragmentRegisterBinding
import ec.com.designtechx.carshop.ui.activity.LoginActivity
import ec.com.designtechx.carshop.ui.view_model.RegisterVM
import ec.com.designtechx.carshop.ui.vm_factory.RegisterVMFactory
import ec.com.designtechx.carshop.utils.Results
import ec.com.designtechx.carshop.utils.ValidateField
import kotlinx.coroutines.flow.collect

class RegisterFragment : Fragment() {
    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    private val validateField by lazy { ValidateField.instance!! }

    private val viewModel by lazy {
        ViewModelProvider(this, RegisterVMFactory()).get(RegisterVM::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        eventClick()
        listenChangeEditText()
        listenVM()
    }

    private fun openLoginFragment() {
        showAlert(getString(R.string.msj_confirmation_email))
        findNavController().navigate(R.id.loginFragment)
    }

    private fun eventClick() {
        binding.btRegister.setOnClickListener { register() }
    }

    private fun register() {
        if (validateField()) {
            waitProcess(true)
            lifecycleScope.launchWhenStarted { viewModel.register(getUser()) }
        }
    }

    private fun getUser():User{
        val email = binding.txtEmail.text.toString()
        val name = binding.txtName.text.toString()
        val password = binding.txtPassword.text.toString()
        return User("", email, password, name)
    }

    private fun validateField(): Boolean {
        return validateField.validateNotEmpty(binding.txtName, binding.tilName, requireContext())
                && validateField.validateEmail(binding.txtEmail, binding.tilEmail, requireContext())
                && validateField.validatePassword(
            binding.txtPassword, binding.tilPassword, requireContext()
        )
    }

    private fun listenChangeEditText() {
        binding.txtPassword.doAfterTextChanged {
            validateField.validatePassword(
                binding.txtPassword, binding.tilPassword, requireContext()
            )
        }
        binding.txtEmail.doAfterTextChanged {
            validateField.validateEmail(
                binding.txtEmail, binding.tilEmail, requireContext()
            )
        }
        binding.txtName.doAfterTextChanged {
            validateField.validateNotEmpty(
                binding.txtName, binding.tilName, requireContext()
            )
        }
    }

    private fun showAlert(message: String?) {
        try {
            waitProcess(false)
            viewModel.resetResult()
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Aviso")
            builder.setMessage(message)
            builder.setPositiveButton("Aceptar", null)
            val dialog = builder.create()
            dialog.show()
        } catch (e: Exception) {
        }
    }

    private fun waitProcess(value: Boolean) {
        (requireActivity() as LoginActivity).binding.progressBar.visibility =
            if (value) View.VISIBLE else View.INVISIBLE
        binding.btRegister.isEnabled = !value
    }

    private fun listenVM() {
        lifecycleScope.launchWhenStarted {
            viewModel.createResult.collect {
                when (it) {
                    Results.OK -> openLoginFragment()
                    Results.FAIL_REGISTER -> showAlert(getString(R.string.fail_register))
                    Results.FAIL_CREATE -> showAlert(getString(R.string.fail_create))
                    Results.FAIL_SEND_CONFIRMATION -> showAlert(getString(R.string.not_send_confirmation))
                    else -> {
                    }
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}