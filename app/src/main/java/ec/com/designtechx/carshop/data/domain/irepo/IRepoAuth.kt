package ec.com.designtechx.taxidriver.data.irepo

import android.app.Activity
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.PhoneAuthProvider
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

interface IRepoAuth {
    suspend fun login(email: String, password: String): Flow<String>
    suspend fun getId(): Flow<String>
    suspend fun existSession(): Flow<Boolean>
    fun logout()
    suspend fun getCurrentUser(): Flow<FirebaseUser>
    suspend fun resetPassword(email:String): Flow<Boolean>
    suspend fun register(email:String,password: String): Flow<String>
    suspend fun sendEmailConfirmation(): Flow<Boolean>

}