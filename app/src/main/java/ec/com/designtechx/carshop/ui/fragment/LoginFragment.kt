package ec.com.designtechx.carshop.ui.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import ec.com.designtechx.carshop.R
import ec.com.designtechx.carshop.databinding.FragmentLoginBinding
import ec.com.designtechx.carshop.ui.activity.LoginActivity
import ec.com.designtechx.carshop.ui.activity.MainActivity
import ec.com.designtechx.carshop.ui.view_model.LoginVM
import ec.com.designtechx.carshop.ui.vm_factory.LoginVMFactory
import ec.com.designtechx.carshop.utils.Results
import ec.com.designtechx.carshop.utils.ValidateField
import kotlinx.coroutines.flow.collect

class LoginFragment : Fragment() {
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private val viewModel by lazy {
        ViewModelProvider(this, LoginVMFactory()).get(LoginVM::class.java)
    }

    private val validateField by lazy { ValidateField.instance!! }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        eventClick()
        listenChangeEditText()
        listenVM()
    }

    private fun eventClick() {
        binding.btLogin.setOnClickListener { login() }
        binding.btRecoveryPassword.setOnClickListener { recoveryPassword() }
    }

    private fun login() {
        if (validateField()) {
            waitProcess(true)
            val email = binding.txtEmail.text.toString()
            val password = binding.txtPassword.text.toString()
            lifecycleScope.launchWhenStarted {
                viewModel.login(email, password)
            }
        }
    }

    private fun validateField(): Boolean {
        return validateField.validateEmail(
            binding.txtEmail,
            binding.tilEmail,
            requireContext()
        ) && validateField.validatePassword(
            binding.txtPassword,
            binding.tilPassword,
            requireContext()
        )
    }

    private fun listenChangeEditText() {
        binding.txtPassword.doAfterTextChanged {
            validateField.validatePassword(
                binding.txtPassword,
                binding.tilPassword,
                requireContext()
            )
        }
        binding.txtEmail.doAfterTextChanged {
            validateField.validateEmail(binding.txtEmail, binding.tilEmail, requireContext())
        }
    }

    private fun listenVM() {
        lifecycleScope.launchWhenStarted {
            viewModel.loginResult.collect {
                when (it) {
                    Results.OK -> openMain()
                    Results.NOT_EXISTENT -> showAlert(getString(R.string.not_exist_user))
                    Results.NOT_CONFIRM -> showAlert(getString(R.string.confirme_user))
                    else -> {
                    }
                }
            }
        }
    }


    private fun showAlert(message: String) {
        try {
            waitProcess(false)
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle(getString(R.string.attention))
            builder.setMessage(message)
            builder.setPositiveButton(getString(R.string.m_accept), null)
            val dialog = builder.create()
            dialog.show()
            viewModel.resetLoginResult()
        } catch (e: Exception) {
        }
    }

    private fun waitProcess(value: Boolean) {
        (requireActivity() as LoginActivity).binding.progressBar.visibility =
            if (value) View.VISIBLE else View.INVISIBLE
        binding.btLogin.isEnabled = !value
        binding.btRecoveryPassword.isEnabled = !value
    }

    private fun recoveryPassword() {
        (requireActivity() as LoginActivity).binding.progressBar.visibility = View.INVISIBLE
        findNavController().navigate(R.id.recoveryPasswordFragment)
    }

    private fun openMain() {
        waitProcess(false)
        (requireActivity() as LoginActivity).binding.progressBar.visibility = View.INVISIBLE
        startActivity(Intent(requireContext(), MainActivity::class.java))
        requireActivity().finish()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}