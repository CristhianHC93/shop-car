package ec.com.designtechx.carshop.data.domain.repo

import ec.com.designtechx.carshop.data.domain.irepo.IRepoUser
import ec.com.designtechx.carshop.data.model.User
import ec.com.designtechx.carshop.provider.UserProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow

class RepoUser(private val userProvider: UserProvider):IRepoUser {

    @ExperimentalCoroutinesApi
    override suspend fun create(user:User):Flow<Boolean> = flow {
        userProvider.create(user).collect { emit(it) }
    }

    @ExperimentalCoroutinesApi
    override suspend fun update(user:User):Flow<Boolean> = flow {
        userProvider.update(user).collect { emit(it) }
    }

    @ExperimentalCoroutinesApi
    override suspend fun getUser(idUser:String):Flow<User> = flow {
        userProvider.getUser(idUser).collect { emit(it) }
    }
}