package ec.com.designtechx.carshop.data.model

data class User(var id:String="", val email:String="", val password:String="", val name:String="",var image:String="")