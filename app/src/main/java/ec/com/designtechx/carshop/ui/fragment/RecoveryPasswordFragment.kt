package ec.com.designtechx.carshop.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import ec.com.designtechx.carshop.R
import ec.com.designtechx.carshop.databinding.FragmentRecoveryPasswordBinding
import ec.com.designtechx.carshop.ui.activity.LoginActivity
import ec.com.designtechx.carshop.ui.view_model.RecoveryPasswordVM
import ec.com.designtechx.carshop.ui.vm_factory.RecoveryPasswordVMFactory
import ec.com.designtechx.carshop.utils.Results
import ec.com.designtechx.carshop.utils.ValidateField
import kotlinx.coroutines.flow.collect

class RecoveryPasswordFragment : Fragment() {
    private var _binding: FragmentRecoveryPasswordBinding? = null
    private val binding get() = _binding!!

    private val viewModel by lazy {
        ViewModelProvider(this, RecoveryPasswordVMFactory()).get(RecoveryPasswordVM::class.java)
    }

    private val validateField by lazy { ValidateField.instance!! }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRecoveryPasswordBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        eventClick()
        listenChangeEditText()
        listenVM()
    }

    private fun listenVM() {
        lifecycleScope.launchWhenStarted {
            viewModel.resetResult.collect {
                when (it) {
                    Results.OK -> moveToLogin()
                    Results.FAIL -> showAlert()
                    else -> {
                    }
                }
            }
        }
    }

    private fun moveToLogin() {
        waitProcess(false)
        (requireActivity() as LoginActivity).binding.progressBar.visibility = View.INVISIBLE
        Toast.makeText(requireContext(), getString(R.string.email_sent), Toast.LENGTH_SHORT).show()
        findNavController().navigate(R.id.loginFragment)
    }

    private fun eventClick() {
        binding.btRecoveryPassword.setOnClickListener { recoveryPassword() }
    }

    private fun recoveryPassword() {
        if (validateField()) {
                waitProcess(true)
            (requireActivity() as LoginActivity).binding.progressBar.visibility = View.VISIBLE
            val email = binding.txtEmail.text.toString()
            lifecycleScope.launchWhenStarted { viewModel.resetPassword(email) }
        }
    }

    private fun listenChangeEditText() {
        binding.txtEmail.doAfterTextChanged {
            validateField.validateEmail(binding.txtEmail, binding.tilEmail, requireContext())
        }
    }

    private fun validateField(): Boolean {
        return validateField.validateEmail(
            binding.txtEmail,
            binding.tilEmail,
            requireContext()
        )
    }

    private fun showAlert() {
        try {
            waitProcess(false)
            (requireActivity() as LoginActivity).binding.progressBar.visibility = View.INVISIBLE
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle(getString(R.string.error))
            builder.setMessage(getString(R.string.unable_send_mail))
            builder.setPositiveButton(getString(R.string.m_accept), null)
            val dialog = builder.create()
            dialog.show()
            viewModel.resetResult()
        } catch (e: Exception) {
        }
    }

    private fun waitProcess(value: Boolean) {
        (requireActivity() as LoginActivity).binding.progressBar.visibility =
            if (value) View.VISIBLE else View.INVISIBLE
        binding.btRecoveryPassword.isEnabled = !value
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}