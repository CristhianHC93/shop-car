package ec.com.designtechx.carshop.data.domain.irepo

import ec.com.designtechx.carshop.data.model.User
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow

interface IRepoUser {
    suspend fun create(user: User): Flow<Boolean>
    suspend fun update(user: User): Flow<Boolean>
    suspend fun getUser(idUser:String): Flow<User>
}