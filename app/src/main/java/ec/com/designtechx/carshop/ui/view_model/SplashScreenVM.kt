package ec.com.designtechx.carshop.ui.view_model

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import ec.com.designtechx.carshop.utils.Results
import ec.com.designtechx.taxidriver.data.irepo.IRepoAuth
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class SplashScreenVM(private val iRepoAuth: IRepoAuth) : ViewModel() {

    private val _existSession = MutableStateFlow(Results.UNANSWERED)

    val existSession: StateFlow<Results> get() = _existSession

    init {
        viewModelScope.launch {
            iRepoAuth.existSession().collect {
                if (it) checkSession() else _existSession.value = Results.NOT_EXISTENT
            }
        }
    }

    private suspend fun checkSession() {
        iRepoAuth.getCurrentUser().collect {
            _existSession.value = if (it.isEmailVerified) Results.OK else Results.NOT_CONFIRM
        }
    }
}