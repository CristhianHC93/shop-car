package ec.com.designtechx.taxidriver.provider

import android.util.Log
import com.google.firebase.auth.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.flow

class AuthProvider {
    private val auth: FirebaseAuth =
        FirebaseAuth.getInstance().apply { setLanguageCode(LANGUAJE) }

    @ExperimentalCoroutinesApi
    fun register(user: String, password: String): Flow<String> = callbackFlow {
        auth.createUserWithEmailAndPassword(user, password).addOnCompleteListener {
            if (it.isSuccessful)
                it.result?.let { r -> offer(r.user?.uid.toString()) }
            else offer("")
        }
        awaitClose { }
    }

    @ExperimentalCoroutinesApi
    fun login(user: String, password: String): Flow<String> = callbackFlow {
        auth.signInWithEmailAndPassword(user, password).addOnCompleteListener {
            if (it.isSuccessful) it.result?.let { r -> offer(r.user.toString()) }
            else {
                Log.e(TAG, it.exception.toString())
                offer("")
            }
        }
        awaitClose { }
    }

    @ExperimentalCoroutinesApi
    fun sendEmailConfirmation(): Flow<Boolean> = callbackFlow {
        auth.currentUser?.let {
            it.sendEmailVerification().addOnCompleteListener { offer(it.isSuccessful) }
        }
        awaitClose { }
    }

    @ExperimentalCoroutinesApi
    fun resetPassword(email: String): Flow<Boolean> = callbackFlow {
        auth.sendPasswordResetEmail(email).addOnCompleteListener { offer(it.isSuccessful) }
        awaitClose { }
    }

    fun getCurrentUser(): Flow<FirebaseUser> = flow {
        emit(auth.currentUser!!)
    }

    fun getIdUser(): Flow<String> = flow {
        auth.currentUser?.let { emit(it.uid) }
    }

    fun existSession(): Flow<Boolean> = flow {
        emit(auth.currentUser != null)
    }

    fun logout() {
        auth.signOut()
    }

    companion object {

        private val TAG = AuthProvider::class.java.simpleName
        private const val LANGUAJE = "es"

        var instance: AuthProvider? = null
            get() {
                if (field == null)
                    field = AuthProvider()
                return field
            }
            private set
    }
}

