package ec.com.designtechx.carshop.ui.activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import ec.com.designtechx.carshop.R
import ec.com.designtechx.carshop.data.model.Car
import ec.com.designtechx.carshop.databinding.ActivityCarCUBinding
import ec.com.designtechx.carshop.ui.view_model.CarCUVM
import ec.com.designtechx.carshop.ui.vm_factory.CarCUVMFactory
import ec.com.designtechx.carshop.utils.Constants
import ec.com.designtechx.carshop.utils.GeneralHelper
import ec.com.designtechx.carshop.utils.Results
import ec.com.designtechx.carshop.utils.ValidateField
import kotlinx.coroutines.flow.collect

class CarCUActivity : AppCompatActivity() {
    lateinit var binding: ActivityCarCUBinding

    private val viewModel by lazy {
        ViewModelProvider(this, CarCUVMFactory()).get(CarCUVM::class.java)
    }
    private var key = ""
    private var category = ""
    private val validateField by lazy { ValidateField.instance!! }
    private val generalHelper by lazy { GeneralHelper.getInstance(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        listenVM()
        configBiding()
        getArgs()
        eventClick()
        listenChangeEditText()
    }

    private fun listenVM() {
        lifecycleScope.launchWhenStarted {
            viewModel.operationResult.collect {
                when (it) {
                    Results.OK -> comeBack()
                    Results.FAIL -> showAlert(getString(R.string.fail_create))
                    else -> {
                    }
                }
            }
        }

        lifecycleScope.launchWhenStarted {
            viewModel.car.collect { if (it.model.isNotEmpty()) fillView(it) }
        }
    }

    private fun comeBack() {
        onBackPressed()
        finish()
    }

    private fun showAlert(message: String) {
        try {
            waitProcess(false)
            val builder = AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.attention))
            builder.setMessage(message)
            builder.setPositiveButton(getString(R.string.m_accept), null)
            val dialog = builder.create()
            dialog.show()
            viewModel.resetResult()
        } catch (e: Exception) {
        }
    }

    private fun fillView(car: Car) {
        binding.txtPrice.setText(car.price)
        binding.txtModel.setText(car.model)
        binding.txtSeats.setText(car.seats)
        binding.cbIsNew.isChecked = car.new == getString(R.string.m_new)
        configFieldDescription(car.category)
        when (car.category) {
            "commercial" -> binding.txtDesription.setText(car.space_capacity)
            "truck" -> binding.txtDesription.setText(car.available_payload)
            "electric" -> binding.txtDesription.setText(car.battery_capacity)
        }
    }

    private fun eventClick() {
        binding.btAdd.setOnClickListener { add() }
    }

    private fun add() {
        if (validateField()) {
            waitProcess(true)
            if (key.isEmpty()) createCategory() else updateCategory()
        }
    }

    private fun createCategory() =
        lifecycleScope.launchWhenStarted { viewModel.createCar(getCar()) }

    private fun updateCategory() =
        lifecycleScope.launchWhenCreated { viewModel.updateCar(getCar(), key) }

    private fun getCar(): Car {
        val date = if (key.isEmpty()) generalHelper.getDate() else viewModel.car.value.date
        val model = binding.txtModel.text.toString()
        val price = binding.txtPrice.text.toString()
        val seats = binding.txtSeats.text.toString()
        val new =
            if (binding.cbIsNew.isChecked) getString(R.string.m_new) else getString(R.string.used)
        val dataCategory = if (category.isEmpty()) viewModel.car.value.category else category
        return Car(seats, price, new, model, date, dataCategory).apply {
            when (dataCategory) {
                "commercial" -> space_capacity = binding.txtDesription.text.toString()
                "truck" -> available_payload = binding.txtDesription.text.toString()
                "electric" -> battery_capacity = binding.txtDesription.text.toString()
            }
        }
    }


    private fun getArgs() {
        intent.extras?.let {
            key = it.getString(Constants.KEY_CAR, "")
            category = it.getString(Constants.KEY_CATEGORY, "")
            if (key.isNotEmpty()) configUpdate() else configCreate()
        }
    }

    private fun configUpdate() {
        lifecycleScope.launchWhenStarted { viewModel.getCar(key) }
        binding.btAdd.text = getString(R.string.update)
    }

    private fun configCreate() {
        lifecycleScope.launchWhenStarted { viewModel.getCar(key) }
        binding.btAdd.text = getString(R.string.create)
        configFieldDescription(category)
    }

    private fun configFieldDescription(category: String) {
        when (category) {
            "commercial" -> binding.tilDescription.hint = getString(R.string.space_capacity)
            "truck" -> binding.tilDescription.hint = getString(R.string.available_payload)
            "electric" -> binding.tilDescription.hint = getString(R.string.battery_capacity)
            else -> binding.tilDescription.visibility = View.GONE
        }
    }

    private fun validateField(): Boolean {
        return validateField.validateNotEmpty(binding.txtModel, binding.tilModel, this) &&
                validateField.validateNotEmpty(binding.txtPrice, binding.tilPrice, this) &&
                validateField.validateNotEmpty(binding.txtSeats, binding.tilSeats, this)
    }

    private fun listenChangeEditText() {
        binding.txtDesription.doAfterTextChanged {
            validateField.validateNotEmpty(
                binding.txtDesription,
                binding.tilDescription,
                this
            )
        }
        binding.txtModel.doAfterTextChanged {
            validateField.validateNotEmpty(
                binding.txtModel,
                binding.tilModel,
                this
            )
        }
        binding.txtPrice.doAfterTextChanged {
            validateField.validateNotEmpty(
                binding.txtPrice,
                binding.tilPrice,
                this
            )
        }
        binding.txtSeats.doAfterTextChanged {
            validateField.validateNotEmpty(
                binding.txtSeats,
                binding.tilSeats,
                this
            )
        }
    }

    private fun waitProcess(value: Boolean) {
        binding.progressBar.visibility = if (value) View.VISIBLE else View.INVISIBLE
        binding.btAdd.isEnabled = !value
    }

    private fun configBiding() {
        binding = ActivityCarCUBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

}