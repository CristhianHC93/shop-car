package ec.com.designtechx.carshop.ui.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import ec.com.designtechx.carshop.R
import ec.com.designtechx.carshop.data.model.Car
import ec.com.designtechx.carshop.databinding.ItemCarBinding
import ec.com.designtechx.carshop.ui.activity.CarCUActivity
import ec.com.designtechx.carshop.ui.view_model.CarVM
import ec.com.designtechx.carshop.utils.Constants

class CarAdapter(
    options: FirebaseRecyclerOptions<Car>,
    private val context: Context, private val viewModel: CarVM
) : FirebaseRecyclerAdapter<Car, CarAdapter.ViewHolder>(options) {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding = ItemCarBinding.bind(view)
        internal val txtDate = binding.txtDate
        internal val txtIsNew = binding.txtIsNew
        internal val txtModel = binding.txtModel
        internal val txtPrice = binding.txtPrice
        internal val txtSeats = binding.txtSeats
        internal val txtCategory = binding.txtCategory
        internal val txtDesription = binding.txtDesription
        internal val tilDescription = binding.tilDescription
        internal val imgDelete = binding.imgDelete
        internal val imgUpdate = binding.imgUpdate

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_car, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int,
        car: Car
    ) {
        val key = getRef(position).key!!
        holder.txtDate.setText(car.date)
        holder.txtIsNew.setText(car.new)
        holder.txtModel.setText(car.model)
        holder.txtPrice.setText(car.price)
        holder.txtSeats.setText(car.seats)
        holder.txtCategory.setText(car.category_name)
        validateDescription( holder, car)
        if (car.category == "electric") holder.imgUpdate.visibility = View.GONE
        holder.imgUpdate.setOnClickListener { openActivity(key) }
        holder.imgDelete.setOnClickListener { delete(key) }
    }

    private fun validateDescription(holder: ViewHolder, car: Car) {
        when (car.category) {
            "commercial" -> configDescription(holder,car.space_capacity,context.getString(R.string.space_capacity))
            "truck" -> configDescription(holder,car.available_payload,context.getString(R.string.available_payload))
            "electric" -> configDescription(holder,car.battery_capacity,context.getString(R.string.battery_capacity))
            else -> holder.tilDescription.visibility = View.GONE
        }
    }

    private fun configDescription(holder: ViewHolder,value:String,hint:String) {
        holder.txtDesription.setText(value)
        holder.tilDescription.hint = hint
    }

    private fun delete(key: String) = viewModel.delete(key)

    private fun openActivity(key: String) {
        val intent = Intent(context, CarCUActivity::class.java).apply {
            putExtra(Constants.KEY_CAR, key)
        }
        context.startActivity(intent)
    }
}