package ec.com.designtechx.carshop.ui.view_model

import androidx.lifecycle.ViewModel
import com.firebase.ui.database.FirebaseRecyclerOptions
import ec.com.designtechx.carshop.data.domain.irepo.IRepoCategory
import ec.com.designtechx.carshop.data.model.Category

class CategoryVM(private val iRepoCategory: IRepoCategory) : ViewModel() {

    fun getOptionsCategory(): FirebaseRecyclerOptions<Category> {
        return iRepoCategory.getOptions()
    }

    fun delete(key: String) = iRepoCategory.delete(key)
}