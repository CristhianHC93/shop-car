package ec.com.designtechx.carshop.ui.view_model

import androidx.lifecycle.ViewModel
import ec.com.designtechx.carshop.utils.Results
import ec.com.designtechx.taxidriver.data.irepo.IRepoAuth
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect

class RecoveryPasswordVM(private val iRepoAuth: IRepoAuth) : ViewModel() {
    private val _resetResult = MutableStateFlow(Results.UNANSWERED)
    val resetResult: StateFlow<Results> get() = _resetResult


    suspend fun resetPassword(email: String) {
        iRepoAuth.resetPassword(email)
            .collect { _resetResult.value = if (it) Results.OK else Results.FAIL }
    }

    fun resetResult() {
        _resetResult.value = Results.UNANSWERED
    }
}