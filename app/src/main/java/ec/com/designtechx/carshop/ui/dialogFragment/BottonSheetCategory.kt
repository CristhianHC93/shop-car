package ec.com.designtechx.carshop.ui.dialogFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.firebase.database.FirebaseDatabase
import ec.com.designtechx.carshop.data.model.Category
import ec.com.designtechx.carshop.databinding.BottonSheetCategoryBinding
import ec.com.designtechx.carshop.ui.activity.MainActivity
import ec.com.designtechx.carshop.ui.adapter.CategoryForCarAdapter

class BottonSheetCategory : BottomSheetDialogFragment() {
    private var _binding: BottonSheetCategoryBinding? = null
    private val binding get() = _binding!!

    private lateinit var categoryAdapter: CategoryForCarAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = BottonSheetCategoryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configRecyclerAdapter()
    }

    private fun configRecyclerAdapter() {
        val linearLayoutManager = LinearLayoutManager(requireContext())
        binding.rvCategory.layoutManager = linearLayoutManager
        val query = FirebaseDatabase.getInstance().reference
            .child("category")
        val options = FirebaseRecyclerOptions.Builder<Category>()
            .setQuery(query, Category::class.java)
            .build()

        categoryAdapter = CategoryForCarAdapter(options, this)
        binding.rvCategory.adapter = categoryAdapter
        categoryAdapter.startListening()
        (requireActivity() as MainActivity).binding.progressBar.visibility = View.INVISIBLE
    }

    override fun onStop() {
        super.onStop()
        categoryAdapter.stopListening()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}