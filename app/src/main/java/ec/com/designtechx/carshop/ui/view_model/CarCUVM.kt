package ec.com.designtechx.carshop.ui.view_model

import androidx.lifecycle.ViewModel
import ec.com.designtechx.carshop.data.domain.irepo.IRepoCar
import ec.com.designtechx.carshop.data.domain.irepo.IRepoCategory
import ec.com.designtechx.carshop.data.model.Car
import ec.com.designtechx.carshop.utils.Results
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect

class CarCUVM(private val iRepoCar: IRepoCar, private val iRepoCategory: IRepoCategory) : ViewModel() {
    private val _operationResult = MutableStateFlow(Results.UNANSWERED)
    val operationResult: StateFlow<Results> get() = _operationResult

    private val _car = MutableStateFlow(Car())
    val car: StateFlow<Car> get() = _car

    private suspend fun completeCreate(car: Car) {
        iRepoCar.create(car).collect {
            _operationResult.value = if (it) Results.OK else Results.FAIL
        }
    }

    suspend fun createCar(car: Car){
        iRepoCategory.getCategory(car.category).collect {
            if (it.name.isNotEmpty()) car.apply { category_name = it.name }
            completeCreate(car)
        }
    }

    private suspend fun completeUpdate(car: Car,key: String){
        iRepoCar.update(car, key).collect {
            _operationResult.value = if (it) Results.OK else Results.FAIL
        }
    }

    suspend fun updateCar(car: Car, key: String) {
        iRepoCategory.getCategory(car.category).collect {
            if (it.name.isNotEmpty()) car.apply { category_name = it.name }
            completeUpdate(car,key)
        }


    }

    suspend fun getCar(key: String) {
        iRepoCar.getCar(key).collect {
            _car.value = it
        }
    }

    fun resetResult() {
        _operationResult.value = Results.UNANSWERED
    }
}