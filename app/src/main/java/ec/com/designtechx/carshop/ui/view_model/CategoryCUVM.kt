package ec.com.designtechx.carshop.ui.view_model

import androidx.lifecycle.ViewModel
import ec.com.designtechx.carshop.data.domain.irepo.IRepoCategory
import ec.com.designtechx.carshop.data.model.Category
import ec.com.designtechx.carshop.utils.Results
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect

class CategoryCUVM(private val iRepoCategory: IRepoCategory) : ViewModel() {
    private val _operationResult = MutableStateFlow(Results.UNANSWERED)
    val operationResult:StateFlow<Results> get() = _operationResult

    private val _category = MutableStateFlow(Category())
    val category:StateFlow<Category> get() = _category

    suspend fun createCategory(category: Category) {
        iRepoCategory.create(category).collect {
            _operationResult.value = if (it) Results.OK else Results.FAIL
        }
    }

    suspend fun updateCategory(category: Category, key: String) {
        iRepoCategory.update(category, key).collect {
            _operationResult.value = if (it) Results.OK else Results.FAIL
        }
    }

    suspend fun getCategory(key: String) {
        iRepoCategory.getCategory(key).collect {
            _category.value = it
        }
    }

    fun resetResult() {
        _operationResult.value = Results.UNANSWERED
    }
}