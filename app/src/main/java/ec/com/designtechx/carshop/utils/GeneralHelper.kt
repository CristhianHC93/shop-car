package ec.com.designtechx.carshop.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import java.text.SimpleDateFormat
import java.util.*


class GeneralHelper private constructor(var context: Context) {

    fun isOnlineNet(): Boolean {
        try {
            val connectivityManager =
                (context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val activeNetwork = connectivityManager.activeNetwork ?: return false
                val networkCapabilities =
                    connectivityManager.getNetworkCapabilities(activeNetwork) ?: return false
                return when {
                    networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                    networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                    networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                    else -> false
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return false
    }


    //Metod for set color of all the itemMenu of  menu
    fun setColorItemMenu(drawableList: List<Drawable>, color: Int) {
        drawableList.forEach { drawable ->
            drawable.mutate()
            drawable.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(
                color,
                BlendModeCompat.SRC_ATOP
            )
        }
    }


    fun appInstalled(uri: String): Boolean {
        val pm = context.packageManager
        return try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES)
            true
        } catch (e: PackageManager.NameNotFoundException) {
            false
        }
    }

    fun getDate(): String {
        val format = "yyyy-MM-dd"
        return getDateWithFormat(format)
    }

    @SuppressLint("SimpleDateFormat")
    fun getDateWithFormat(formato: String): String {
        val calendar: Calendar = Calendar.getInstance()
        val date: Date = calendar.time
        val sdf = SimpleDateFormat(formato)
        //sdf.setTimeZone(TimeZone.getTimeZone(zonaHoraria))
        return sdf.format(date)
    }

    companion object {
        private var instance: GeneralHelper? = null

        @JvmStatic
        fun getInstance(context: Context): GeneralHelper {
            if (instance == null)
                instance = GeneralHelper(context)
            return instance!!
        }
    }
}