package ec.com.designtechx.carshop.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import ec.com.designtechx.carshop.R
import ec.com.designtechx.carshop.databinding.FragmentAuthBinding

class AuthFragment : Fragment() {
    private var _binding: FragmentAuthBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAuthBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        eventClick()
    }

    private fun eventClick() {
        binding.btLogin.setOnClickListener { moveToFragment(R.id.loginFragment) }
        binding.btRegister.setOnClickListener { moveToFragment(R.id.registerFragment) }
    }

    private fun moveToFragment(fragment: Int) {
        findNavController().navigate(fragment)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}