package ec.com.designtechx.carshop.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import ec.com.designtechx.carshop.databinding.FragmentCarBinding
import ec.com.designtechx.carshop.ui.activity.MainActivity
import ec.com.designtechx.carshop.ui.adapter.CarAdapter
import ec.com.designtechx.carshop.ui.dialogFragment.BottonSheetCategory
import ec.com.designtechx.carshop.ui.view_model.CarVM
import ec.com.designtechx.carshop.ui.vm_factory.CarVMFactory

class CarFragment : Fragment() {
    private var _binding: FragmentCarBinding? = null
    private val binding get() = _binding!!

    private val viewModel by lazy {
        ViewModelProvider(this, CarVMFactory()).get(CarVM::class.java)
    }

    private lateinit var carAdapter: CarAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCarBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        eventClick()
    }

    private fun eventClick() {
        binding.btAdd.setOnClickListener { selectCategory() }
    }

    private fun selectCategory() {
        val modalBottomSheetFragment = BottonSheetCategory()
        modalBottomSheetFragment.show(childFragmentManager, modalBottomSheetFragment.tag)
    }

    override fun onStart() {
        super.onStart()
        (requireActivity() as MainActivity).binding.progressBar.visibility = View.VISIBLE
        configRecyclerAdapter()
    }

    private fun configRecyclerAdapter() {
        val linearLayoutManager = LinearLayoutManager(requireContext())
        binding.rvCar.layoutManager = linearLayoutManager
        carAdapter = CarAdapter(viewModel.getOptionsCar(), requireContext(), viewModel)
        binding.rvCar.adapter = carAdapter
        carAdapter.startListening()
        (requireActivity() as MainActivity).binding.progressBar.visibility = View.INVISIBLE
    }

    override fun onStop() {
        super.onStop()
        carAdapter.stopListening()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}