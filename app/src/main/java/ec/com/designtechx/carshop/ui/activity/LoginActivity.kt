package ec.com.designtechx.carshop.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import ec.com.designtechx.carshop.R
import ec.com.designtechx.carshop.databinding.ActivityLoginBinding
import ec.com.designtechx.carshop.utils.GeneralHelper

class LoginActivity : AppCompatActivity() {
    lateinit var binding: ActivityLoginBinding
    private lateinit var navController: NavController

    private val generalHelper by lazy { GeneralHelper.getInstance(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        configBiding()
        setupToolBarAndNavigation()
    }

    private fun configBiding() {
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    private fun setupToolBarAndNavigation() {
        setSupportActionBar(binding.toolBar)
        val navHostFragment =
            supportFragmentManager.findFragmentById(binding.navAuthFragment.id) as NavHostFragment
        navController = navHostFragment.navController
        NavigationUI.setupActionBarWithNavController(this, navController)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        menu.findItem(R.id.exit).isVisible = false
        menu.findItem(R.id.profile).isVisible = false
        return true
    }
}