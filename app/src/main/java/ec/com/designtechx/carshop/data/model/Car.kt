package ec.com.designtechx.carshop.data.model

data class Car(
    val seats: String = "",
    val price: String="",
    val new: String="",
    val model: String="",
    val date: String="",
    val category:String="",
    var battery_capacity: String = "",
    var available_payload: String = "",
    var space_capacity: String = "",
    var category_name:String="",

)
