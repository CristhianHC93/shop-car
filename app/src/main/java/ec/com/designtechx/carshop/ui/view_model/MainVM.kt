package ec.com.designtechx.carshop.ui.view_model

import androidx.lifecycle.ViewModel
import ec.com.designtechx.taxidriver.data.irepo.IRepoAuth

class MainVM(private val iRepoAuth: IRepoAuth) : ViewModel() {

    fun logout(){
        iRepoAuth.logout()
    }
}