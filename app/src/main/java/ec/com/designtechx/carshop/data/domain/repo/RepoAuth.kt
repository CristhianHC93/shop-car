package ec.com.designtechx.taxidriver.data.repo

import com.google.firebase.auth.FirebaseUser
import ec.com.designtechx.taxidriver.data.irepo.IRepoAuth
import ec.com.designtechx.taxidriver.provider.AuthProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow

class RepoAuth(private val authProvider: AuthProvider) : IRepoAuth {


    @ExperimentalCoroutinesApi
    override suspend fun login(email: String, password: String): Flow<String> = flow {
        authProvider.login(email, password).collect { emit(it) }
    }

    override suspend fun getId(): Flow<String> = flow {
        authProvider.existSession().collect { session ->
            if (session) authProvider.getIdUser().collect { id -> emit(id) }
        }
    }

    override suspend fun existSession() = flow { authProvider.existSession().collect { emit(it) } }
    override fun logout() = authProvider.logout()
    override suspend fun getCurrentUser(): Flow<FirebaseUser> = flow{
        authProvider.getCurrentUser().collect { emit(it) }
    }

    @ExperimentalCoroutinesApi
    override suspend fun resetPassword(email:String): Flow<Boolean> = flow{
        authProvider.resetPassword(email).collect { emit(it) }
    }

    @ExperimentalCoroutinesApi
    override suspend fun register(email:String,password: String): Flow<String> = flow{
        authProvider.register(email,password).collect { emit(it) }
    }

    @ExperimentalCoroutinesApi
    override suspend fun sendEmailConfirmation(): Flow<Boolean> = flow{
        authProvider.sendEmailConfirmation().collect { emit(it) }
    }

}