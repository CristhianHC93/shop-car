package ec.com.designtechx.carshop.ui.view_model

import androidx.lifecycle.ViewModel
import ec.com.designtechx.carshop.utils.Results
import ec.com.designtechx.taxidriver.data.irepo.IRepoAuth
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect

class LoginVM(private val iRepoAuth: IRepoAuth) : ViewModel() {
    private val _loginResult = MutableStateFlow(Results.UNANSWERED)
    val loginResult: StateFlow<Results> get() = _loginResult

    suspend fun login(email: String, password: String) {
        iRepoAuth.login(email, password)
            .collect {
                if (it.isNotEmpty()) checkSession() else _loginResult.value = Results.NOT_EXISTENT
            }
    }

    private suspend fun checkSession() {
        iRepoAuth.getCurrentUser().collect {
            _loginResult.value = if (it.isEmailVerified) Results.OK else Results.NOT_CONFIRM
        }
    }

    fun resetLoginResult() {
        _loginResult.value = Results.UNANSWERED
    }
}
