package ec.com.designtechx.carshop.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import ec.com.designtechx.carshop.R
import ec.com.designtechx.carshop.data.model.Category
import ec.com.designtechx.carshop.databinding.ActivityCategoryCUBinding
import ec.com.designtechx.carshop.ui.view_model.CategoryCUVM
import ec.com.designtechx.carshop.ui.vm_factory.CategoryCUVMFactory
import ec.com.designtechx.carshop.utils.Constants
import ec.com.designtechx.carshop.utils.Results
import ec.com.designtechx.carshop.utils.ValidateField
import kotlinx.coroutines.flow.collect

class CategoryCUActivity : AppCompatActivity() {
    lateinit var binding: ActivityCategoryCUBinding

    private val viewModel by lazy {
        ViewModelProvider(this, CategoryCUVMFactory()).get(CategoryCUVM::class.java)
    }

    private val validateField by lazy { ValidateField.instance!! }
    private var key = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        configBiding()
        getKey()
        eventClick()
        listenChangeEditText()
        listenVM()
    }

    private fun listenVM() {
        lifecycleScope.launchWhenStarted {
            viewModel.operationResult.collect {
                when (it) {
                    Results.OK -> comeBack()
                    Results.FAIL -> showAlert(getString(R.string.fail_create))
                    else -> {
                    }
                }
            }
        }

        lifecycleScope.launchWhenStarted {
            viewModel.category.collect { fillView(it) }
        }
    }

    private fun fillView(category: Category) {
        binding.txtName.setText(category.name)
        binding.txtDescription.setText(category.description)
    }

    private fun comeBack() {
        onBackPressed()
        finish()
    }

    private fun showAlert(message: String) {
        try {
            waitProcess(false)
            val builder = AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.attention))
            builder.setMessage(message)
            builder.setPositiveButton(getString(R.string.m_accept), null)
            val dialog = builder.create()
            dialog.show()
            viewModel.resetResult()
        } catch (e: Exception) {
        }
    }

    private fun waitProcess(value: Boolean) {
        binding.progressBar.visibility = if (value) View.VISIBLE else View.INVISIBLE
        binding.btAdd.isEnabled = !value
    }

    private fun getKey() {
        intent.extras?.let {
            key = it.getString(Constants.KEY_CATEGORY, "")
            if (key.isNotEmpty()) configUpdate()
        }
    }

    private fun configUpdate() {
        lifecycleScope.launchWhenStarted { viewModel.getCategory(key) }
        if (key == "commercial" || key == "truck" || key == "electric")
            binding.txtName.isEnabled = false
        binding.btAdd.text = getString(R.string.update)
    }

    private fun configBiding() {
        binding = ActivityCategoryCUBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    private fun eventClick() {
        binding.btAdd.setOnClickListener { add() }
    }

    private fun listenChangeEditText() {
        binding.txtName.doAfterTextChanged {
            validateField.validateNotEmpty(
                binding.txtName, binding.tilName, this
            )
        }
        binding.txtDescription.doAfterTextChanged {
            validateField.validateNotEmpty(
                binding.txtDescription, binding.tilDescription, this
            )
        }
    }

    private fun add() {
        if (validateField()) {
            waitProcess(true)
            if (key.isEmpty()) createCategory() else updateCategory()
        }
    }

    private fun createCategory() =
        lifecycleScope.launchWhenStarted { viewModel.createCategory(getCategory()) }

    private fun updateCategory() =
        lifecycleScope.launchWhenCreated { viewModel.updateCategory(getCategory(), key) }

    private fun getCategory(): Category {
        val name = binding.txtName.text.toString()
        val description = binding.txtDescription.text.toString()
        return Category(name, description)
    }

    private fun validateField(): Boolean {
        return validateField.validateNotEmpty(
            binding.txtName, binding.tilName, this
        ) && validateField.validateNotEmpty(binding.txtDescription, binding.tilDescription, this)
    }

}