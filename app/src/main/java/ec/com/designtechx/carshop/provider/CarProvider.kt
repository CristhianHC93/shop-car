package ec.com.designtechx.carshop.provider

import android.util.Log
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.*
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import ec.com.designtechx.carshop.data.model.Car
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

class CarProvider private constructor(){
    private val dataBase: DatabaseReference = Firebase.database.reference.child(V_CAR)

    @ExperimentalCoroutinesApi
    fun create(car: Car): Flow<Boolean> = callbackFlow {
        val key = dataBase.push().key
        dataBase.child(key!!).setValue(car).addOnCompleteListener {
            if (it.isSuccessful) offer(true)
            else {
                Log.w(TAG, it.exception.toString())
                offer(false)
            }
        }
        awaitClose { }
    }

    @ExperimentalCoroutinesApi
    fun update(car: Car, key: String): Flow<Boolean> = callbackFlow {
        val map = mapOf(
            Pair("seats", car.seats),
            Pair("price", car.price),
            Pair("new", car.new),
            Pair("model", car.model),
            Pair("date", car.date),
            Pair("category", car.category),
            Pair("category_name", car.category_name),
            Pair("battery_capacity", car.battery_capacity),
            Pair("available_payload", car.available_payload),
            Pair("space_capacity", car.space_capacity)
        )
        dataBase.child(key).updateChildren(map).addOnCompleteListener {
            if (it.isSuccessful) offer(true)
            else {
                Log.w(TAG, it.exception.toString())
                offer(false)
            }
        }
        awaitClose { }
    }

    fun delete(key: String) {
        dataBase.child(key).removeValue()
    }

    @ExperimentalCoroutinesApi
    fun getCar(key: String): Flow<Car> = callbackFlow {
        dataBase.child(key).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) offer(snapshot.getValue(Car::class.java))
                else offer(Car())
            }

            override fun onCancelled(error: DatabaseError) {
                Log.w(TAG, error.message)
                offer(Car())
            }
        })
        awaitClose { }
    }


    fun getOptions(): FirebaseRecyclerOptions<Car> {
        val query = FirebaseDatabase.getInstance().reference
            .child(V_CAR)
        return FirebaseRecyclerOptions.Builder<Car>()
            .setQuery(query, Car::class.java)
            .build()
    }

    companion object {
        private val TAG = CarProvider::class.java.simpleName
        private const val V_CAR = "car"
        var instance: CarProvider? = null
            get() {
                if (field == null)
                    field = CarProvider()
                return field
            }
            private set
    }
}