package ec.com.designtechx.carshop.data.domain.repo

import com.firebase.ui.database.FirebaseRecyclerOptions
import ec.com.designtechx.carshop.data.domain.irepo.IRepoCar
import ec.com.designtechx.carshop.data.model.Car
import ec.com.designtechx.carshop.provider.CarProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow

class RepoCar(private val carProvider: CarProvider):IRepoCar {
    @ExperimentalCoroutinesApi
    override suspend fun create(car: Car): Flow<Boolean> = flow {
        carProvider.create(car).collect {
            emit(it)
        }
    }

    @ExperimentalCoroutinesApi
    override suspend fun update(car: Car, key: String): Flow<Boolean> = flow {
        carProvider.update(car, key).collect {
            emit(it)
        }
    }

    override fun getOptions(): FirebaseRecyclerOptions<Car> {
        return carProvider.getOptions()
    }

    @ExperimentalCoroutinesApi
    override suspend fun getCar(key: String): Flow<Car> = flow {
        carProvider.getCar(key).collect { emit(it) }
    }

    override fun delete(key: String) {
        carProvider.delete(key)
    }
}