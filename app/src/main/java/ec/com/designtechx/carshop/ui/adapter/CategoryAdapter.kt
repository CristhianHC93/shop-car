package ec.com.designtechx.carshop.ui.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import ec.com.designtechx.carshop.R
import ec.com.designtechx.carshop.data.model.Category
import ec.com.designtechx.carshop.databinding.ItemCategoryBinding
import ec.com.designtechx.carshop.ui.activity.CategoryCUActivity
import ec.com.designtechx.carshop.ui.view_model.CategoryVM
import ec.com.designtechx.carshop.utils.Constants

class CategoryAdapter(
    options: FirebaseRecyclerOptions<Category>,
    private val context: Context, private val viewModel: CategoryVM?
) : FirebaseRecyclerAdapter<Category, CategoryAdapter.ViewHolder>(options) {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding = ItemCategoryBinding.bind(view)
        internal val txtTitle = binding.txtTitle
        internal val txtDescription = binding.txtDescription
        internal val imgDelete = binding.imgDelete

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_category, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int,
        category: Category
    ) {
        val key = getRef(position).key
        validateDefault(key,holder)
        holder.txtTitle.text = category.name
        holder.txtDescription.text = category.description
        holder.itemView.setOnClickListener { openActivity(key!!) }
        holder.imgDelete.setOnClickListener { delete(key!!) }
    }

    private fun validateDefault(key: String?, holder: CategoryAdapter.ViewHolder) {
        if (key == "commercial" || key == "truck" || key == "electric")
            holder.imgDelete.visibility = View.GONE
    }

    private fun delete(key: String) = viewModel!!.delete(key)

    private fun openActivity(key: String) {
        val intent = Intent(context, CategoryCUActivity::class.java).apply {
            putExtra(Constants.KEY_CATEGORY, key)
        }
        context.startActivity(intent)
    }
}