package ec.com.designtechx.carshop.ui.vm_factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ec.com.designtechx.carshop.data.domain.repo.RepoUser
import ec.com.designtechx.carshop.provider.UserProvider
import ec.com.designtechx.carshop.ui.view_model.LoginVM
import ec.com.designtechx.carshop.ui.view_model.RegisterVM
import ec.com.designtechx.taxidriver.data.repo.RepoAuth
import ec.com.designtechx.taxidriver.provider.AuthProvider

class RegisterVMFactory :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return RegisterVM(
            RepoAuth(AuthProvider.instance!!),
            RepoUser(UserProvider.instance!!)
        ) as T
    }
}