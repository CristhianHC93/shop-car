package ec.com.designtechx.carshop.data.domain.repo

import com.firebase.ui.database.FirebaseRecyclerOptions
import ec.com.designtechx.carshop.data.domain.irepo.IRepoCategory
import ec.com.designtechx.carshop.data.model.Category
import ec.com.designtechx.carshop.provider.CategoryProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow

class RepoCategory(private val categoryProvider: CategoryProvider) : IRepoCategory {

    @ExperimentalCoroutinesApi
    override suspend fun create(category: Category): Flow<Boolean> = flow {
        categoryProvider.create(category).collect {
            emit(it)
        }
    }

    @ExperimentalCoroutinesApi
    override suspend fun update(category: Category, key: String): Flow<Boolean> = flow {
        categoryProvider.update(category, key).collect {
            emit(it)
        }
    }

    override fun getOptions(): FirebaseRecyclerOptions<Category> {
        return categoryProvider.getOptions()
    }

    @ExperimentalCoroutinesApi
    override suspend fun getCategory(key: String): Flow<Category> = flow {
        categoryProvider.getCategory(key).collect { emit(it) }
    }

    override fun getCategorySingle(key: String): Category {
        return categoryProvider.getCategorySingle(key)
    }


    override fun delete(key: String) {
        categoryProvider.delete(key)
    }
}