package ec.com.designtechx.carshop.utils

import android.content.Context
import android.util.Patterns
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import ec.com.designtechx.carshop.R

class ValidateField private constructor() {

    fun validateNotEmpty(
        textInputEditText: TextInputEditText,
        textInputLayout: TextInputLayout,
        context: Context
    ): Boolean {
        var returnMethod = true
        if (textInputEditText.text!!.isEmpty()) {
            textInputLayout.error = context.getString(R.string.required_field)
            returnMethod = false
        } else
            textInputLayout.error = ""
        return returnMethod
    }

    fun validatePassword(
        textInputEditText: TextInputEditText,
        textInputLayout: TextInputLayout,
        context: Context
    ): Boolean {
        var returnMethod = true
        when {
            textInputEditText.text!!.length in 1..5 -> {
                textInputLayout.error = context.getString(R.string.min_legth_password)
                returnMethod = false
            }
            textInputEditText.text!!.isEmpty() -> {
                textInputLayout.error = context.getString(R.string.required_field)
                returnMethod = false
            }
            else -> textInputLayout.error = ""
        }
        return returnMethod
    }

    fun validateEmail(
        textInputEditText: TextInputEditText,
        textInputLayout: TextInputLayout,
        context: Context
    ): Boolean {
        var returnMethod = false
        if (validateNotEmpty(textInputEditText, textInputLayout, context)) {
            if (Patterns.EMAIL_ADDRESS.matcher(textInputEditText.text.toString()).matches()) {
                textInputLayout.error = ""
                returnMethod = true
            } else {
                textInputLayout.error = context.getString(R.string.email_invalid)
            }
        }
        return returnMethod
    }

    fun validateDouble(
        textInputEditText: TextInputEditText,
        textInputLayout: TextInputLayout,
        context: Context
    ): Boolean {
        var returnMethod = false
        if (validateNotEmpty(textInputEditText, textInputLayout, context)) {
            try {
                val value = textInputEditText.text.toString().toDouble()
                if (value > 0.0) {
                    returnMethod = true
                    textInputLayout.error = ""
                } else textInputLayout.error = context.getString(R.string.more_zero)
            } catch (e: Exception) {
                textInputLayout.error = context.getString(R.string.enter_price)
            }
        }
        return returnMethod
    }


    companion object {
        var instance: ValidateField? = null
            get() {
                if (field == null)
                    field = ValidateField()
                return field
            }
            private set
    }
}