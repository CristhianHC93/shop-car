package ec.com.designtechx.carshop.provider

import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import ec.com.designtechx.carshop.data.model.User
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

class UserProvider {

    private val dataBase: DatabaseReference = Firebase.database.reference.child(V_USER)

    @ExperimentalCoroutinesApi
    fun create(user: User): Flow<Boolean> = callbackFlow {
        val map = mapOf(
            Pair("name", user.name),
            Pair("email", user.email),
            Pair("image", user.image)
        )
        dataBase.child(user.id).setValue(map).addOnCompleteListener {
            if (it.isSuccessful) offer(true)
            else {
                Log.e(TAG, it.exception.toString())
                offer(false)
            }

        }
        awaitClose { }
    }

    @ExperimentalCoroutinesApi
    fun getUser(idUser: String): Flow<User> = callbackFlow {
        dataBase.child(idUser).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    val user = snapshot.getValue(User::class.java)
                    offer(user)
                } else offer(User("-1"))
            }

            override fun onCancelled(error: DatabaseError) {
                Log.w(TAG, error.message)
                offer(User("-1"))
            }
        })
    }

    @ExperimentalCoroutinesApi
    fun update(user: User): Flow<Boolean> = callbackFlow {
        val map = mapOf(
            Pair("name", user.name),
            Pair("email", user.email),
            Pair("image", user.image)
        )
        dataBase.child(user.id).updateChildren(map)
            .addOnCompleteListener { offer(it.isSuccessful) }
        awaitClose { }
    }

    companion object {
        private val TAG = UserProvider::class.java.simpleName
        private const val V_USER = "user"
        var instance: UserProvider? = null
            get() {
                if (field == null)
                    field = UserProvider()
                return field
            }
            private set
    }
}