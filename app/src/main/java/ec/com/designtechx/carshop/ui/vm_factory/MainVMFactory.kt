package ec.com.designtechx.carshop.ui.vm_factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ec.com.designtechx.carshop.ui.view_model.MainVM
import ec.com.designtechx.taxidriver.data.repo.RepoAuth
import ec.com.designtechx.taxidriver.provider.AuthProvider

class MainVMFactory :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainVM(
            RepoAuth(AuthProvider.instance!!)
        ) as T
    }
}