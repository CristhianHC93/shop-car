package ec.com.designtechx.carshop.provider

import android.util.Log
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.*
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import ec.com.designtechx.carshop.data.model.Category
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

class CategoryProvider private constructor() {

    private val dataBase: DatabaseReference = Firebase.database.reference.child(V_CATEGORY)

    @ExperimentalCoroutinesApi
    fun create(category: Category): Flow<Boolean> = callbackFlow {
        val key = dataBase.push().key
        dataBase.child(key!!).setValue(category).addOnCompleteListener {
            if (it.isSuccessful) offer(true)
            else {
                Log.w(TAG, it.exception.toString())
                offer(false)
            }
        }
        awaitClose { }
    }

    @ExperimentalCoroutinesApi
    fun update(category: Category, key: String): Flow<Boolean> = callbackFlow {
        val map = mapOf(
            Pair("name", category.name),
            Pair("description", category.description)
        )
        dataBase.child(key).updateChildren(map).addOnCompleteListener {
            if (it.isSuccessful) offer(true)
            else {
                Log.w(TAG, it.exception.toString())
                offer(false)
            }
        }
        awaitClose { }
    }

    fun delete(key:String){
        dataBase.child(key).removeValue()
    }

    @ExperimentalCoroutinesApi
    fun getCategory(key: String): Flow<Category> = callbackFlow {
        dataBase.child(key).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) offer(snapshot.getValue(Category::class.java))
                else offer(Category())
            }

            override fun onCancelled(error: DatabaseError) {
                Log.w(TAG, error.message)
                offer(Category())
            }
        })
        awaitClose { }
    }

    fun getCategorySingle(key: String): Category  {
        var category= Category()
        dataBase.child(key).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) category = snapshot.getValue(Category::class.java)!!
            }

            override fun onCancelled(error: DatabaseError) {
                Log.w(TAG, error.message)
            }
        })
        return category
    }


    fun getOptions(): FirebaseRecyclerOptions<Category> {
        val query = FirebaseDatabase.getInstance().reference
            .child(V_CATEGORY)
        return FirebaseRecyclerOptions.Builder<Category>()
            .setQuery(query, Category::class.java)
            .build()
    }

    companion object {
        private val TAG = CategoryProvider::class.java.simpleName
        private const val V_CATEGORY = "category"
        var instance: CategoryProvider? = null
            get() {
                if (field == null)
                    field = CategoryProvider()
                return field
            }
            private set
    }
}