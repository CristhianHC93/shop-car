package ec.com.designtechx.carshop.ui.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ec.com.designtechx.carshop.R
import ec.com.designtechx.carshop.data.model.Category
import ec.com.designtechx.carshop.databinding.ItemCategoryForCarBinding
import ec.com.designtechx.carshop.ui.activity.CarCUActivity
import ec.com.designtechx.carshop.utils.Constants

class CategoryForCarAdapter(
    options: FirebaseRecyclerOptions<Category>,
    private val context: BottomSheetDialogFragment
) : FirebaseRecyclerAdapter<Category, CategoryForCarAdapter .ViewHolder>(options) {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding = ItemCategoryForCarBinding.bind(view)
        internal val txtCategory = binding.txtCategory
        internal val imgSelect = binding.imgSelect

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_category_for_car, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int,
        category: Category
    ) {
        val key = getRef(position).key
        holder.txtCategory.setText(category.name)
        holder.imgSelect.setOnClickListener { openActivity(key!!) }
    }
    
    private fun openActivity(key: String) {
        val intent = Intent(context.requireContext(), CarCUActivity::class.java).apply {
            putExtra(Constants.KEY_CATEGORY, key)
        }
        context.startActivity(intent)
        context.dismiss()
    }
}